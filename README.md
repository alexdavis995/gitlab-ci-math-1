Advanced Programming

Author@Alex Davis 
ID# 1503051
Lab 1 Assessment

Objectives

Gitlab

Issues
Settings


Gitlab CI


Overview
This program
Creates a shell script named “math-program.sh” that 
accepts 3 numbers as command line arguments (NUM1, NUM2, NUM3)
Calculates a result that is equal to (NUM1 ^ NUM2) + NUM3 x NUM2
Prints the calculation it is about to perform in the format (NUM1 ^ NUM2) + NUM3 x NUM2
The program prints the result of the calculation 
The program writes only the result (eg. 5) to a file myresult.txt in a folder build i.e. build/myresult.txt
The program allows the user to choose a different file name by changing and environment variable named OUTPUT_FILE_NAME
